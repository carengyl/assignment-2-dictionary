%include "lib.inc"

%define next_element 8

section .text
global find_word

; Принимает: указатель на нуль-терминированную строку, указатель на начало словаря.
; Проходит по всему словарю в поисках подходящего ключа. 
; Если подходящее вхождение найдено, 
; вернёт адрес начала вхождения в словарь (не значения), 
; иначе вернёт 0.
find_word:
    push r8
    push r9

    mov r8, rdi
    mov r9, rsi
    .loop:
        test r9, r9
        jz .error
        mov rdi, r8
        add rsi, next_element
        call string_equals
        test rax, rax
        jnz .end
        mov rsi, [r9]
        mov r9, rsi
        jmp .loop
    .end:
        mov rax, r9
        jmp .ret
    .error:
        xor rax, rax
    .ret:
        pop r9
        pop r8
        ret