%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define buffer_size 256
%define next_element 8
%define newline 0xA


section .rodata
    read_error_message: db "Failed to read word", 0
    not_found_error_message: db "Not found", 0

section .bss
    buffer: resb buffer_size

global _start

section .text

_start:
    mov rdi, buffer
    mov rsi, buffer_size
    call read_word
    test rax, rax
    jz .read_exception
    push rdx
    mov rdi, buffer
    mov rsi, top
    call find_word
    pop rdx
    test rax, rax
    jz .not_found_exception
    mov rdi, rax
    add rdi, next_element
    add rdi, rdx
    inc rdi
    call print_string
    mov rdi, 0 ; In case of success
    jmp exit

    .not_found_exception:
        mov rdi, not_found_error_message
        push 1 ; In case of not found exception
        jmp .error_out

    .read_exception:
        mov rdi, read_error_message
        push 2 ; In case of read exception

    .error_out:
        call print_error
        pop rdi ; pop error code
        jmp exit
