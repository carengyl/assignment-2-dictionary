section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov byte [rsp], dil
    mov rdx, 1
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    syscall
    inc rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 10
    mov rax, rdi
    xor r8, r8
    dec rsp
    mov byte[rsp], 0
    inc r8

.loop:
    xor rdx, rdx
    div rcx
    add dl, 48
    dec rsp
    mov [rsp], dl
    inc r8
    test rax, rax
    jnz .loop
.end:
    mov rdi, rsp
    push r8
    call print_string
    pop r8
    add rsp, r8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print
    push rdi 
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    push rsi

    call string_length
    pop rsi
    mov rdx, rax ; rdx = string1 length

    mov rdi, rsi
    call string_length ; rax == string2 length
    pop rsi
    pop rdi

    cmp rax, rdx
    jne .not_equal ; if lengths are different

    mov rcx, rax 
    cmp rcx, 0
    je .equal ; if lengths are = 0

    xor rax, rax
    xor rdx, rdx

.loop:

    mov al, [rdi + rcx - 1]
    mov dl, [rsi + rcx - 1]
    cmp al, dl
    jne .not_equal

    dec rcx ; next char
    cmp rcx, 0
    jg .loop ; if not EOF

.equal:
    mov rax, 1
    ret

.not_equal:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    jz .eof
    mov al, [rsp]
    .eof:
        inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ; Initialize rcx with the sum of rdi and rsi
    mov rcx, rdi
    add rcx, rsi
    dec rcx
    ; Copy rdi to rsi, set rdx to zero
    mov rsi, rdi
    xor rdx, rdx
.loop:
    ; Save registers on the stack
    push rdi
    push rsi
    push rcx
    push rdx
    ; Call read_char function
    call read_char
    ; Restore registers from the stack
    pop rdx
    pop rcx
    pop rsi
    pop rdi

    ; Check if read character is EOF
    cmp al, 0  
    je .end

    ; Check if read character is space, tab, or newline
    cmp al, 0x20
    je .if_space
    cmp al, 0x09
    je .if_space
    cmp al, 0x0A
    je .if_space

    ; If none of the above, jump to .else
    jmp .else

.if_space: ; Case: read character is '\n' or '\t' or ' '
    cmp rdx, 0
    je .loop
    ; If rdx is not zero, jump to .end
    jmp .end

.else: ; Else
    ; Set rdx to 1, copy read character to [rsi], increment rsi
    mov rdx, 1
    mov byte [rsi], al
    inc rsi
    ; Check if rsi is less than rcx, if so, continue looping
    cmp rsi, rcx
    jl .loop
    ; If rsi is not less than rcx, jump to .error
    jmp .error

.end:
    ; Null-terminate the string at [rsi], calculate length in rdx, and return
    mov byte[rsi], 0
    mov rax, rdi
    sub rsi, rdi
    mov rdx, rsi
    ret

.error:
    ; Return 0 in case of an error
    xor rax, rax
    ret 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ; Initialize rsi with the input pointer, set rax and rdx to zero, and rcx to 10
    mov rsi, rdi
    xor rax, rax
    xor rdx, rdx
    mov rcx, 10
.loop:
    ; Load the character at [rsi] into al
    mov al, [rsi]
    cmp al, '0'
    jl .end ; if read char < 0
    cmp al, '9'
    jg .end ; if read char > 9
    cmp al, 0
    push rax
    je .next_digit ; if number == 0

.mult:
    ; Multiply rdx by rcx, saving the result in rdx:rax
    push rcx
    push rdx
    mov rax, rdx
    mul rcx
    pop rdx
    pop rcx
    mov rdx, rax
    xor rax, rax

.next_digit:
    ; Pop the original value of rax, convert the ASCII digit to integer, add to rdx
    pop rax
    sub al, '0'
    add rdx, rax
    inc rsi 
    jmp .loop

.end:
    ; Set rax to the final result, calculate length in rdx, and return
    mov rax, rdx
    sub rsi, rdi
    mov rdx, rsi
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rcx, rcx
    mov al, [rdi]
    cmp al, '-'
    je .negative
    jmp .uint

.negative:
    inc rcx ; if negative, sign flag (rcx)+1
    inc rdi

.uint:
    push rcx
    call parse_uint
    pop rcx
    cmp rdx, 0
    je .end ; if error while parsing uint
    cmp rcx, 0 
    je .end ; if not negative
    neg rax
    inc rdx

.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi 
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi ; get string length

    inc rax ; string_length += 1
    cmp rdx, rax
    jl .error ; if buffer is smaller than string length
    mov rcx, rax 
    add rcx, rdi
.loop:
    mov al, [rdi] ; copy characters
    mov [rsi], al
    inc rdi
    inc rsi
    cmp rdi, rcx
    jl .loop ; if (len buffer) != 0
    mov rax, rcx
    jmp .end
.error:
    xor rax, rax
.end:
    ret

print_error:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rax, 1
    mov  rsi, rdi
    mov  rdi, 2
    syscall
    ret
