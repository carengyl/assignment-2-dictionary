%include "colon.inc"

section .rodata

    colon "animal", el4
    db "bear", 0

    colon "bird", el3
    db "eagle", 0

    colon "reptile", el2
    db "snake", 0

    colon "bug", el1
    db "spider", 0