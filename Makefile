NASM = nasm
NASM_FLAGS = -felf64 -o
LD = ld
LD_FLAGS = -o

%.o: %.asm
	$(NASM) $(NASM_FLAGS) $@ $<

main: lib.o dict.o main.o
	$(LD) $(LD_FLAGS) $@ $^

dict.o: dict.asm lib.inc
	$(NASM) $(NASM_FLAGS) $@ $<

main.o: main.asm lib.inc dict.inc words.inc
	$(NASM) $(NASM_FLAGS) $@ $<

.PHONY: test clean

clean:
	rm -f *.o
	rm -f main

test: main
	python3 ./test.py
