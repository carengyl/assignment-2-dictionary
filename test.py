from subprocess import *

NOT_FOUND_ERROR_MESSAGE = "Not found"
READ_ERROR_MESSAGE = "Failed to read word"

tests = {
    "animal": ("bear", ""),
    "bird": ("eagle", ""),
    "reptile": ("snake", ""),
    "bug": ("spider", ""),
    "":("", NOT_FOUND_ERROR_MESSAGE),
    "sdasad": ("", NOT_FOUND_ERROR_MESSAGE),
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequa235235235235326234664364366653": ("", NOT_FOUND_ERROR_MESSAGE)
}

def build():
    try:
        run(["make", "main"], stdout=DEVNULL, stderr=DEVNULL).check_returncode()
        print("Successfully compiled")
    except:
        print("Something went wrong")
        exit()

def run_tests():
    build()
    passed = 0
    for test in tests.keys():
        test_run = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        stdout, stderr = test_run.communicate(input=test.encode())
        stdout = stdout.decode().strip()
        stderr = stderr.decode().strip()

        if stdout == tests[test][0] and stderr == tests[test][1]:
            passed += 1
        else: 
            print("Test failed")
            print("Input:", test)
            print("Expected output:", tests[test][0])
            print("stdout:", stdout)
            print("Expected error:", tests[test][1])
            print("stderr:", stderr)
            print("Exit code:", test_run.returncode)

    if passed == len(tests.keys()):
        print("All test passed.")
    else:
        print(len(tests.keys()) - passed, "test(s) failed.")

if __name__ == "__main__":
    run_tests()
